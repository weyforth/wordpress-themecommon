<?php

  function get_current_git_commit($branch = null) {
    if(!$branch) $branch = get_current_git_branch();

    if ( $hash = @file(ABSPATH . '../../.git/refs/heads/' . $branch)) {
      return $hash[0];
    } else {
      if ( $hash = @file(ABSPATH . '../../REVISION')){
        return $hash[0];
      }else{
        return false;
      }
    }
  }

  function get_current_git_branch() {
    $stringfromfile = @file(ABSPATH . '../../.git/HEAD' );

    if(!$stringfromfile) return '';

    $firstLine = $stringfromfile[0];
    $explodedstring = explode("/", $firstLine, 3);
    $branchname = $explodedstring[2];

    return trim($branchname);
  }